﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnShowProperties = new System.Windows.Forms.Button();
            this.btnXReport = new System.Windows.Forms.Button();
            this.btnZReport = new System.Windows.Forms.Button();
            this.lblResult = new System.Windows.Forms.Label();
            this.tbResult = new System.Windows.Forms.TextBox();
            this.btnBeep = new System.Windows.Forms.Button();
            this.btnPrintString = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnShowProperties
            // 
            this.btnShowProperties.Location = new System.Drawing.Point(12, 12);
            this.btnShowProperties.Name = "btnShowProperties";
            this.btnShowProperties.Size = new System.Drawing.Size(129, 23);
            this.btnShowProperties.TabIndex = 0;
            this.btnShowProperties.Text = "ShowProperties";
            this.btnShowProperties.UseVisualStyleBackColor = true;
            this.btnShowProperties.Click += new System.EventHandler(this.btnShowProperties_Click);
            // 
            // btnXReport
            // 
            this.btnXReport.Location = new System.Drawing.Point(12, 41);
            this.btnXReport.Name = "btnXReport";
            this.btnXReport.Size = new System.Drawing.Size(129, 23);
            this.btnXReport.TabIndex = 1;
            this.btnXReport.Text = "XReport";
            this.btnXReport.UseVisualStyleBackColor = true;
            this.btnXReport.Click += new System.EventHandler(this.btnXReport_Click);
            // 
            // btnZReport
            // 
            this.btnZReport.Location = new System.Drawing.Point(12, 70);
            this.btnZReport.Name = "btnZReport";
            this.btnZReport.Size = new System.Drawing.Size(129, 23);
            this.btnZReport.TabIndex = 2;
            this.btnZReport.Text = "ZReport";
            this.btnZReport.UseVisualStyleBackColor = true;
            this.btnZReport.Click += new System.EventHandler(this.btnZReport_Click);
            // 
            // lblResult
            // 
            this.lblResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(9, 206);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(62, 13);
            this.lblResult.TabIndex = 3;
            this.lblResult.Text = "Результат:";
            // 
            // tbResult
            // 
            this.tbResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbResult.Location = new System.Drawing.Point(12, 222);
            this.tbResult.Name = "tbResult";
            this.tbResult.Size = new System.Drawing.Size(773, 20);
            this.tbResult.TabIndex = 4;
            // 
            // btnBeep
            // 
            this.btnBeep.Location = new System.Drawing.Point(12, 99);
            this.btnBeep.Name = "btnBeep";
            this.btnBeep.Size = new System.Drawing.Size(129, 23);
            this.btnBeep.TabIndex = 5;
            this.btnBeep.Text = "Beep";
            this.btnBeep.UseVisualStyleBackColor = true;
            this.btnBeep.Click += new System.EventHandler(this.btnBeep_Click);
            // 
            // btnPrintString
            // 
            this.btnPrintString.Location = new System.Drawing.Point(12, 128);
            this.btnPrintString.Name = "btnPrintString";
            this.btnPrintString.Size = new System.Drawing.Size(129, 23);
            this.btnPrintString.TabIndex = 6;
            this.btnPrintString.Text = "PrintString";
            this.btnPrintString.UseVisualStyleBackColor = true;
            this.btnPrintString.Click += new System.EventHandler(this.btnPrintString_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(147, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(279, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Тестовый чек";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(147, 41);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(279, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "Проверить  отправленный фискальный документ";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(443, 56);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(47, 20);
            this.numericUpDown1.TabIndex = 9;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(147, 70);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(279, 23);
            this.button3.TabIndex = 10;
            this.button3.Text = "Распечатать фискальный документ";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(147, 99);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(279, 23);
            this.button4.TabIndex = 11;
            this.button4.Text = "Повторить документ";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 254);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnPrintString);
            this.Controls.Add(this.btnBeep);
            this.Controls.Add(this.tbResult);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.btnZReport);
            this.Controls.Add(this.btnXReport);
            this.Controls.Add(this.btnShowProperties);
            this.Name = "Form1";
            this.Text = "Тест драйвера ФР";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnShowProperties;
        private System.Windows.Forms.Button btnXReport;
        private System.Windows.Forms.Button btnZReport;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.TextBox tbResult;
        private System.Windows.Forms.Button btnBeep;
        private System.Windows.Forms.Button btnPrintString;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}

