﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DrvFRLib;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Driver = new DrvFR();
        }
        DrvFR Driver;
        private void UpdateResult()
        {
            tbResult.Text = string.Format("Результат: {0}, {1}", Driver.ResultCode, Driver.ResultCodeDescription);
        }
        private void btnXReport_Click(object sender, EventArgs e)
        {
            Driver.PrintReportWithoutCleaning();
            UpdateResult();
        }

        private void btnZReport_Click(object sender, EventArgs e)
        {
            Driver.PrintReportWithCleaning();
            UpdateResult();
        }

        private void btnShowProperties_Click(object sender, EventArgs e)
        {
            Driver.ShowProperties();
            UpdateResult();
        }

        private void btnBeep_Click(object sender, EventArgs e)
        {
            Driver.Beep();
            UpdateResult();
        }

        private void btnPrintString_Click(object sender, EventArgs e)
        {
            Driver.StringForPrinting = "Тестовая строка";
            Driver.PrintString();
            UpdateResult();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int result;
            decimal sumSale = 0;

            Driver.Password = 30;
            Driver.CheckType = 0;
            Driver.OpenCheck();
           

            Driver.CustomerEmail = "tarasov_al@cki.com.ru";
            Driver.FNSendCustomerEmail();




            sumSale += AddString(1,"Falcon 9", 10000, 2);

            sumSale += AddString(2, "Tesla", 1000, 1);

            sumSale += AddString(3, "Биография Илона Маска", 10, 5);


            Driver.Summ1 = 0;//наличные
            Driver.Summ2 = sumSale; //карта
            Driver.Summ3 = 0;
            Driver.Summ4 = 0;
            Driver.DiscountOnCheck = 0;
            Driver.Tax1 = 1;
            Driver.Tax2 = 0;
            Driver.Tax3 = 0;
            Driver.Tax4 = 0;
            Driver.StringForPrinting = "==============================";

            result = Driver.CloseCheck();
            
            UpdateResult();
            
        }


        private decimal AddString(int pos, string name, decimal price, int qunatity)
        {
            //регистрация операции
            Driver.Quantity = qunatity;
            Driver.Price = price;
            Driver.Department = 0;
            Driver.Tax1 = 1;
            Driver.Tax2 = 0;
            Driver.Tax3 = 0;
            Driver.Tax4 = 0;
            Driver.StringForPrinting = $"//{name}";
            Driver.FNDiscountOperation();

            //печать

            var tax = Math.Round(((double) price * (double) qunatity /1.18)*0.18,2);

            Driver.CarryStrings = true;//не ясно как работает
            Driver.StringForPrinting = $"{pos}. {name}.";
            Driver.PrintString();

            Driver.StringForPrinting = $"Цена:  {price}";
            Driver.PrintString();

            Driver.StringForPrinting = $"Налог: 18% = {tax}";
            Driver.PrintString();


            Driver.StringForPrinting = $"Итого: {price* qunatity} рублей ";
            Driver.PrintString();


            UpdateResult();
            return price * qunatity;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Driver.Password = 30;
            Driver.CheckingType = 0;
            Driver.CheckFM();
            UpdateResult();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Driver.Password = 30;
            Driver.DocumentNumber = (int)numericUpDown1.Value;

            if (Driver.FNFindDocument() == 0)
            {
                if (Driver.OFDTicketReceived)
                {
                    MessageBox.Show("Квитанция из ОФД получена");
                }
                else
                {
                    MessageBox.Show("Квитанция из ОФД НЕ пришла");
                }
            }
            UpdateResult();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Driver.Password = 30;
            Driver.DocumentNumber = (int)numericUpDown1.Value;

            Driver.FNPrintDocument();
            UpdateResult();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Driver.RepeatDocument();
            // Driver.ReprintSlipDocument(); //надо изучить как работает
            UpdateResult();
        }

    }
}
